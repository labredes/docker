# Docker Installation

## Uninstall old versions

Older versions of Docker are called docker, docker.io , or docker-engine. If these are installed, uninstall them:
```bash
sudo apt remove docker docker-engine docker.io containerd runc
```

## Install using the repository
Before you install Docker Engine - Community for the first time on a new host machine, you need to set up the Docker repository. Afterward, you can install and update Docker from the repository.
Update the apt package index:
```bash
sudo apt update
```

### Install packages to allow apt to use a repository over HTTPS:
```bash 
sudo apt install apt-transport-https ca-certificates curl gnupg2 software-properties-common
```

### Add Docker’s official GPG key:
```bash 
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
```

### Set up the stable repository. 
```bash 
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
```

### Update the apt package index.
```bash 
sudo apt update
```

### Install the latest version of Docker Engine - Community and containerd.

```bash
sudo apt install docker-ce docker-ce-cli containerd.io
```

## Post-Installation (Linux)

The Docker daemon binds to a Unix socket instead of a TCP port. By default that Unix socket is owned by the user root and other users can only access it using sudo. To use as a non-root user, it is possible to create a Unix group called docker and add users to it. When the Docker daemon starts, it creates a Unix socket accessible by members of the docker group.
Create the docker group.
```bash 
sudo  groupadd docker
```
Add the linux user to the docker group.
```bash 
sudo  usermod -aG docker $USER
```
Then, it is necessary to log out and log back to reload the group membership.
